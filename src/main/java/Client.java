import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.bind.util.ISO8601Utils;
import com.google.gson.stream.JsonWriter;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.*;


public class Client {
    public static int port = 11000;
    public static String address = "157.245.129.238";

    public static void main(String[] args) {
        try {
            InetAddress addr = InetAddress.getByName(address);
            System.out.println("Connecting " + address + ":" + port + "...");
            Socket socket = new Socket(addr, port);
            PrintWriter out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), StandardCharsets.UTF_8));
            Scanner sc = new Scanner(System.in);
            System.out.println("[Request]:");

            Fields fields = new Fields();
            fields.MessageType = 0;
            fields.Multiply = 0;
            fields.Sum = 0;
            Gson gson = new Gson();
            out.println(gson.toJson(fields));
            out.flush();
            Thread.sleep(1000);
            System.out.println("true");

            String jsonJavaRootObject;
            byte[] messageByte = new byte[100];
            boolean end = false;
            StringBuilder dataString = new StringBuilder();
            int cnt = 0;
            int count = 0;
            while (socket.isConnected()) {
                try {
                    DataInputStream in = new DataInputStream(socket.getInputStream());
                    int bytesRead = 0;

                    messageByte[0] = in.readByte();
                    messageByte[1] = in.readByte();
                    ByteBuffer byteBuffer = ByteBuffer.wrap(messageByte, 0, 2);

                    int bytesToRead = byteBuffer.getShort();
                    System.out.println("About to read " + bytesToRead + " octets");

                    //The following code shows in detail how to read from a TCP socket
                    int[] array = new int[3];
                    while (!end) {
                        bytesRead = in.read(messageByte);
                        dataString.append(new String(messageByte, 0, bytesRead));
                        System.out.println(dataString);
                        String q = dataString.delete(0, count).toString().replaceAll("[{}\", :]", "");
                        System.out.println(dataString.length());
                        System.out.println(bytesToRead);
                        System.out.println(q);
                        System.out.println(Arrays.toString(array));
                        count = dataString.length();
                        fields.MessageType = sc.nextInt();
                        fields.Multiply = sc.nextInt();
                        fields.Sum = sc.nextInt();
                        out.println(gson.toJson(fields));
                        out.flush();
                        Thread.sleep(1000);
                        System.out.println("true " + cnt);
                        cnt++;
                        if (dataString.length() == bytesToRead) {
                            end = true;
                        }
                    }

                    System.out.println("MESSAGE: " + dataString);
                } catch (Exception e) {
                    e.printStackTrace();
                    System.exit(0);
                }
            }
//            while (!jsonJavaRootObject.isEmpty()) {
//
//                System.out.println(true);
//                Solution.x = Integer.parseInt(jsonJavaRootObject.get("X").toString());
//                Solution.y = Integer.parseInt(jsonJavaRootObject.get("Y").toString());
//                int ans = Solution.Task1();
//                String line = String.valueOf(ans);
//                out.println(line);
//                out.flush();
//                System.out.println(jsonJavaRootObject);
//                System.out.println(jsonJavaRootObject.get("X"));
//                System.out.println(jsonJavaRootObject.get("Type"));
//                System.out.println(jsonJavaRootObject.get("Type").toString());
//                if (jsonJavaRootObject.get("Type").toString().equals("2.0")) {
//                    System.out.println("Ответ неверный");
//                    break;
//                }
//                if (jsonJavaRootObject.get("Type").toString().equals("3.0")) {
//                    System.out.println("Произошла ошибка");
//                    break;
//                }
//                System.out.println("[Answer]:" + line);
//            }
//            System.out.println("Ответ верный");
            socket.close();

        } catch (IOException | InterruptedException x) {
            System.out.println("Ошибка ввода/вывода");
            x.printStackTrace();
        }
    }
}